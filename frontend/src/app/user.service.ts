import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  loginStatus: boolean;

  // Dependency Injection for HttpClient
  constructor(private http: HttpClient) {
    this.loginStatus = false;
  }

  // Fetch all users
  getAllUsers(): any {
    return this.http.get('http://localhost:8085/getAllUsers');
  }

  // Get user by ID
  getUserById(userId: any): any {
    return this.http.get('http://localhost:8085/getUserById/' + userId);
  }

  // Fetch all departments
  getAllDepartments(): any {
    return this.http.get('http://localhost:8085/getAllDepartments');
  }

  // Register a new user
  registerUser(user: any) {
    return this.http.post('http://localhost:8086/addUser', user);
  }

  // Delete a user by ID
  deleteUser(userId: any) {
    return this.http.delete('http://localhost:8085/deleteUserById/' + userId);
  }

  getAllCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  // Update user details
  updateUser(user: any) {
    return this.http.put('http://localhost:8085/updateUser', user);
  }

  // User login
  userLogin(emailId: any, password: any) {
    return this.http
      .get('http://localhost:8086/userLogin/' + emailId + '/' + password)
      .toPromise();
  }

  // Set user login status to true
  isUserLoggedIn() {
    this.loginStatus = true;
  }

  // Set user login status to false
  isUserLoggedOut() {
    this.loginStatus = false;
  }

  // Get the current login status
  getLoginStatus(): boolean {
    return this.loginStatus;
  }

  // forgot password code

  getUserByEmail(emailId: any) {
    return this.http.get('http://localhost:8086/getUserByEmail/' + emailId);
  }

  sendOtpToEmail(emailId: any) {
    return this.http.get('http://localhost:8086/sendOtpToEmail/' + emailId);
  }

  updateUserPassword(user: any) {
    return this.http.put('http://localhost:8086/updateUserPassword', user);
  }
}
