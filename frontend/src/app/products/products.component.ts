import { Component } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrl: './products.component.css',
})
export class ProductsComponent {
  emailId: any;
  products: any;
  cartProducts: any;

  constructor() {
    this.cartProducts = [];
    this.emailId = localStorage.getItem('emailId');

    this.products = [
      {
        id: 117,
        name: 'Haier 7 Kg Semi Automatic Top Load Washing Machine with Vortex Pulsator & Magic Filter (HTW70-178BKN, White)',
        price: 57999.0,
        description: 'Standard EMI starting from ₹561/month',
        imgsrc:
          'https://d2xamzlzrdbdbn.cloudfront.net/products/9efd8ef9-ca5f-42b2-bb77-2f0b87f3754123010707.jpg',
      },
      {
        id: 117,
        name: 'Haier 7 Kg Semi Automatic Top Load Washing Machine with Vortex Pulsator & Magic Filter (HTW70-178BKN, White)',
        price: 57999.0,
        description: 'Standard EMI starting from ₹561/month',
        imgsrc:
          'https://d2xamzlzrdbdbn.cloudfront.net/products/dcfc0a0d-dbf0-4ccc-947a-07e2cb76348022050519_182x182.jpg',
      },
      {
        id: 117,
        name: 'Haier 7 Kg Semi Automatic Top Load Washing Machine with Vortex Pulsator & Magic Filter (HTW70-178BKN, White)',
        price: 57999.0,
        description: 'Standard EMI starting from ₹561/month',
        imgsrc:
          'https://d2xamzlzrdbdbn.cloudfront.net/products/0ac3b798-fb16-42f9-bf0b-a6ebe2b6708223301311_182x182.jpg',
      },
      {
        id: 117,
        name: 'Haier 7 Kg Semi Automatic Top Load Washing Machine with Vortex Pulsator & Magic Filter (HTW70-178BKN, White)',
        price: 57999.0,
        description: 'Standard EMI starting from ₹561/month',
        imgsrc:
          'https://d2xamzlzrdbdbn.cloudfront.net/products/b35ec172-a477-4edf-8207-b4bb1b1660a1_182x182.jpg',
      },
      {
        id: 117,
        name: 'Haier 7 Kg Semi Automatic Top Load Washing Machine with Vortex Pulsator & Magic Filter (HTW70-178BKN, White)',
        price: 57999.0,
        description: 'Standard EMI starting from ₹561/month',
        imgsrc:
          'https://d2xamzlzrdbdbn.cloudfront.net/products/9c763b58-4104-43d5-85fd-b344dac0de1a23040446_182x182.jpg',
      },
      {
        id: 117,
        name: 'Haier 7 Kg Semi Automatic Top Load Washing Machine with Vortex Pulsator & Magic Filter (HTW70-178BKN, White)',
        price: 57999.0,
        description: 'Standard EMI starting from ₹561/month',
        imgsrc:
          'https://d2xamzlzrdbdbn.cloudfront.net/products/ee2c6245-1fc0-4583-b15a-0a4a0f35366623031016_182x182.jpg',
      },
      {
        id: 117,
        name: 'Haier 7 Kg Semi Automatic Top Load Washing Machine with Vortex Pulsator & Magic Filter (HTW70-178BKN, White)',
        price: 57999.0,
        description: 'Standard EMI starting from ₹561/month',
        imgsrc:
          'https://d2xamzlzrdbdbn.cloudfront.net/products/2229a8d3-167b-407e-92c4-8b8cab0e2ef723301317_182x182.jpg',
      },
      {
        id: 117,
        name: 'Haier 7 Kg Semi Automatic Top Load Washing Machine with Vortex Pulsator & Magic Filter (HTW70-178BKN, White)',
        price: 57999.0,
        description: 'Standard EMI starting from ₹561/month',
        imgsrc:
          'https://d2xamzlzrdbdbn.cloudfront.net/products/163c3a7f-8c2d-49d9-a512-b9bcdade783a23281319_182x182.jpg',
      },
      {
        id: 117,
        name: 'Haier 7 Kg Semi Automatic Top Load Washing Machine with Vortex Pulsator & Magic Filter (HTW70-178BKN, White)',
        price: 57999.0,
        description: 'Standard EMI starting from ₹561/month',
        imgsrc:
          'https://d2xamzlzrdbdbn.cloudfront.net/products/f4ecb0f1-199a-4b61-b65a-69b9502fbab321220531_182x182.jpg',
      },
    ];
  }

  addToCart(product: any) {
    this.cartProducts.push(product);
    localStorage.setItem('cartProducts', JSON.stringify(this.cartProducts));
  }
}
