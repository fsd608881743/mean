package com.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDao;
import com.model.User;

@RestController
@CrossOrigin(origins="http://localhost:4200")

public class UserController {
	
	@Autowired
	UserDao userDao;
		
	@GetMapping("getAllUsers")
	public List<User> getAllUsers() {
	    return userDao.getAllUsers();
	}

	@GetMapping("getUserById/{userId}")
	public User getUserById(@PathVariable("userId") int userId) {
	    return userDao.getUserById(userId);
	}

	@GetMapping("getUserByName/{userName}")
	public User getUserByName(@PathVariable("userName") String userName) {
	    return userDao.getUserByName(userName);
	}

	@PostMapping("addUser")
	public User addUser(@RequestBody User user) {
	    return userDao.addUser(user);
	}

	@PutMapping("updateUser")
	public User updateUser(@RequestBody User user) {
	    return userDao.updateUser(user);
	}

	@DeleteMapping("deleteUserById/{userId}")
	public String deleteUserById(@PathVariable("userId") int userId) {
	    userDao.deleteUserById(userId);
	    return "User with UserId: " + userId + ", Deleted Successfully!!!";
	}

//	@GetMapping("UserByEmailIdAndPassword/{uEmailId}/{upassword}")
//	public List<User> UserByEmailIdAndPassword(@PathVariable("uEmailId") String EmailId,@PathVariable("upassword") String Password) {	
//		List<User> userList = userDao.UserByEmailIdAndPassword(EmailId,Password);		
//		return userList;
//	}
	
	@GetMapping("userLogin/{emailId}/{password}")
	public User userLogin(@PathVariable("emailId") String emailId, @PathVariable("password") String password){
		return userDao.userLogin(emailId, password);
	}
	
	
	// Forgot Password code
	
	@GetMapping("getUserByEmail/{emailId}")
	public User getUserByEmail(@PathVariable("emailId") String emailId){
		return userDao.getUserByEmail(emailId);
	}

	
	@GetMapping("sendOtpToEmail/{emailId}")
	public String sendOtpToEmail(@PathVariable("emailId") String emailId){		
		return userDao.sendOtpToEmail(emailId);
	}
	
	@PutMapping("updateUserPassword")
	public User updateUserPassword(@RequestBody User user){
		return userDao.updateUserPassword(user);
	}



}
