package com.dao;

import java.util.Collections;

import java.util.List;


import javax.mail.MessagingException;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.User;
import com.utils.EmailUtil;
import com.utils.OtpUtil;
// Changed from Employee to User

@Service
public class UserDao { // Changed from EmployeeDao to UserDao

    @Autowired
    UserRepository userRepository;
    
	
	@Autowired
	private OtpUtil otpUtil;
	
	@Autowired
	private EmailUtil emailUtil;
// Changed from EmployeeRepository to UserRepository

    public List<User> getAllUsers() { // Changed from getAllEmployees to getAllUsers
        return userRepository.findAll(); // Changed from employeeRepository to userRepository
    }

    public User getUserById(int userId) { // Changed from getEmployeeById to getUserById
        return userRepository.findById(userId).orElse(null); // Changed from employeeRepository to userRepository
    }

    public User getUserByName(String userName) { // Changed from getEmployeeByName to getUserByName
        return userRepository.findByName(userName); // Changed from employeeRepository to userRepository
    }

    public User addUser(User user) {
	    // Encrypt the password before saving
	    String encryptedPassword = new BCryptPasswordEncoder().encode(user.getPassword());
	    user.setPassword(encryptedPassword);
	    
	    // Save the student with the encrypted password
	    User user1 = userRepository.save(user);
	    return user1;
	}

    public User updateUser(User user) { // Changed from updateEmployee to updateUser
        return userRepository.save(user); // Changed from employeeRepository to userRepository
    }

    public void deleteUserById(int userId) { // Changed from deleteEmployeeById to deleteUserById
        userRepository.deleteById(userId); // Changed from employeeRepository to userRepository
    }

//    public User UserByEmailIdAndPassword(String emailId, String password) {
//		User userList = userRepository.findByEmailAndPassword(emailId,password);
//		return userList;
//	}
    public User userLogin(String emailId, String password) {
		
		User user = userRepository.findByEmail(emailId);
		
		if(user != null){
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			if (passwordEncoder.matches(password, user.getPassword())) {
	            
	            return user;
	        }
		}
		
		return null;
	}


// forgot Passsword code

public String sendOtpToEmail(String emailId) {
	User user = userRepository.findByEmail(emailId);
	String otp = otpUtil.generateOtp();
	if(user != null){	
		try {
			emailUtil.sendOtpEmail(user.getEmailId(), user.getUserName() ,otp);
		} catch (MessagingException e) {
			throw new RuntimeException("Unable to Send OTP");
		}
	}else{
		return "User Not Found!!!...";
	}
	return otp;
}

public User getUserByEmail(String emailId) {
	return userRepository.findByEmail(emailId);
}

public User updateUserPassword(User user) {
	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	String encryptedPassword = passwordEncoder.encode(user.getPassword());
	user.setPassword(encryptedPassword);
	
	return userRepository.save(user);
}



}