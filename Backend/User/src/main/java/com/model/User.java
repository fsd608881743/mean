package com.model;


import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User { // Changed from Employee to User

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // Changed from GenerationType.AUTO to GenerationType.IDENTITY
    private int userId; // Changed from empId to userId
    private String userName; // Changed from empName to userName
    private String gender;
    private String dob; // Changed from doj to dob
    private String country;
    private String emailId;
    private String password; // Added confirmPassword for consistency
    
  
   
    public User() {
    }

    public User(int userId, String userName, String gender, String dob, String country, String emailId,
                String password) { // Changed from (int empId, String empName, ...) to (int userId, String userName, ...)
        this.userId = userId;
        this.userName = userName;
        this.gender = gender;
        this.dob = dob;
        this.country = country;
        this.emailId = emailId;
        this.password = password;
    }

    public int getUserId() { // Changed from getEmpId to getUserId
        return userId;
    }

    public void setUserId(int userId) { // Changed from setEmpId to setUserId
        this.userId = userId;
    }

    public String getUserName() { // Changed from getEmpName to getUserName
        return userName;
    }

    public void setUserName(String userName) { // Changed from setEmpName to setUserName
        this.userName = userName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() { // Changed from getDoj to getDob
        return dob;
    }

    public void setDob(String dob) { // Changed from setDoj to setDob
        this.dob = dob;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//    public String getConfirmPassword() {
//        return confirmPassword;
//    }
//
//    public void setConfirmPassword(String confirmPassword) {
//        this.confirmPassword = confirmPassword;
//    }

   

    @Override
    public String toString() {
        return "User [userId=" + userId + ", userName=" + userName + ", gender=" + gender
                + "]";
    }
}